<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Signature extends Model
{
    /**
     * Field to be mass-assigned.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'body', 'flagged_at'];

    public function scopeIgnoreFlagged($query)
    {
        return $query->where('flagged_at', null);
    }

    /**
     * Updates signature to status flagged
     */

    public function flag()
    {
        return $this->update(['flaggad_at' => Carbon::now()]);
    }

    /**
     * Get the user Gravatar by their email address
     */
    public function getAvatarAttribute()
    {
        return sprintf('https://www.gravatar.com/avatar/%s?s=100', md5($this->email));
    }
}
